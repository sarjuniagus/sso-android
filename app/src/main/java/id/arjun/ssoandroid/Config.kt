package id.arjun.ssoandroid

object Config {

    const val clientId = "kelompok-dasawisma"
    const val userName = "pusdatin"
    const val passWord = "P@ssw0rd"
    const val grandType = "password"
    const val baseUrl = "https://sso.dasawisma.id/auth/realms/simpkk/protocol/openid-connect"
    const val authenticationCodeUrl = "$baseUrl/token"
    const val redirectUri = "maslick://oauthresponse"
}