package id.arjun.ssoandroid

import android.app.Application
import id.arjun.ssoandroid.di.apiModule
import id.arjun.ssoandroid.di.sharedPrefsModule
import org.koin.android.ext.android.startKoin

class SsoLoginApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(apiModule, sharedPrefsModule))
    }
}